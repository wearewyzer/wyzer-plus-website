
  (function($, undefined) {
    "use strict";
    console.log("hello");
    $("#contactSubmitButton").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });

    if ($("form#contact-form").length > 0) {
        $.validate({
            form: "form#contact-form",
            validateOnBlur: true,
            modules: "sanitize",
            scrollToTopOnError: false,
            onSuccess: function($form) {
                submit_form($form, "mailer/submit-contact-form.php");
                return false;
            }
        });
    }
    if ($("form#sign-up-form").length > 0) {
        $.validate({
            form: "form#sign-up-form",
            validateOnBlur: true,
            modules: "sanitize",
            scrollToTopOnError: false,
            onSuccess: function($form) {
                submit_form($form, "mailer/submit-subscribe-form.php");
                return false;
            }
        });
    }
    function submit_form(form, script) {
        var the_form = form;
        the_form.find("button").text("Sending");
        // $.each(the_form.serializeArray(), function() {
        //     form_data[this.name] = this.value;
        // });
        var name = $("input#name").val();
        var email = $("input#email").val();
        var phone = $("input#phone").val();
        //var message = $("textarea#message").val();
        var form_data = {
            'name' : name,
            'email' : email,
            'phone' : phone,
            'message' :"Beta Sign Up"
        };

        var form_json = JSON.stringify(form_data);
        console.log(form_json);
        $.ajax({
            url: script,
            async: true,
            cache: false,
            type: "POST",
            dataType: "json",
            data: {
                data: form_json
            },
            success: function(data) {
                if (data.status === "success") {
                    the_form.trigger("reset");
                    the_form.find("button").text("Sent");
                    var msg = $('<span class="help-block alert-success form-success" style="margin-bottom: 0;margin-top: 1rem"/>').text(data.message);
                    if ($("#contact-form").length > 0) {
                        msg = $('<span class="help-block alert-success form-success" style="margin-bottom: 1rem;margin-top: 1rem"/>').text(data.message);
                        msg.insertBefore(the_form.find("button"));
                    } else {
                        the_form.append(msg);
                    }
                } else if (data.status === "error") {
                    console.error("Error: " + data.message);
                }
            },
            error: function() {
                console.error("Error: Ajax Fatal Error");
            }
        });
    }

})(jQuery);